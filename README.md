# Semantic Web Content Analytics

## Présentation

Projet annuel de M1 IDC.

Groupes :

- [21504598 Clément Vétillard](https://gitlab.com/clementvtrd)
- [21500503 Lucas Tournière](https://gitlab.com/ltourniere)

[Sujet](https://forge.info.unicaen.fr/projects/projets-m2-idc/wiki/Semantic_Webcontent_Analytics)

### Installation

Pour tester le projet, certaines dépendances doivent être satisfaites.

```bash
#optionnel
python3 -m venv .env 
source .env/bin/activate
#installation
pip install -U pip setuptools wheel
pip install -r requirements.txt
python -m spacy download de_core_news_sm
python -m spacy download fr_core_news_sm
python -m spacy download en_core_web_sm
python -m spacy download xx_ent_wiki_sm
```

### Démarrage

```bash
cd <path to project>/src
python manage.py runserver
```
