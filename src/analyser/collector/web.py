from unidecode import unidecode
from html.parser import HTMLParser
from urllib.request import urlopen


def attr_exists(attr_target: tuple, attrs: list):
    """ Check if all value of targeted tag's attributs exists inside current tag's attributs.

    Keyword arguments:
    attr_target --  targeted attribut with values should exists in attrs
    attrs       --  list of attributs with their values to compare with attr_target
    """
    for attr in attrs:
        if attr[0] == attr_target[0]:
            values = attr[1].split(' ')
            for value_target in attr_target[1].split(' '):
                if not value_target in values:
                    return False
            return True
    return False


def attrs_exists(attrs_target: list, attrs: list):
    """ Check if all targeted tag's attributs exists inside tag's attributs

    Keyword arguments:
    attrs_target  --  list of targeted attributs with their values to compare with tag's attributs
    attrs         --  list of attributs with their values to compare with attrs_target
    """
    for attr_target in attrs_target:
        if not attr_exists(attr_target, attrs):
            return False
    return True


class TargetTag(HTMLParser):
    """ HTMLParser that record text content inside targeted tag.
  
    Note: This parser will always ignore script tag's content.
    """

    def __init__(self, tag: str, attrs: list):
        """ TargetTag constructor.

        Keyword arguments:
        tag   --  name of the HTML tag to record (ex: "p" to record all text inside paragraph)
        attrs --  list of tuples to select specific HTML tag (ex: [("id", "specific"), ("class", "text-center")])
        """
        super().__init__()
        self.clear = []
        self.inner_script_tag = False
        self.inner_targeted_tag = False
        self.targeted_tag = tag
        self.targeted_attrs = attrs
        self.similar_tag = -1
        self.data = []

    def handle_starttag(self, tag, attrs):
        """ This method handle start tags.

        It also increments identical tags count to prevent accidental stop record when
        encounter same tag.

        Keyword arguments:
        tag   -- name of the handled HTML tag
        attrs -- list of attributs' tag
        """
        if tag == self.targeted_tag:
            self.similar_tag += 1
            if attrs_exists(self.targeted_attrs, attrs):
                self.inner_targeted_tag = True
        elif tag == "script":
            self.inner_script_tag = True

    def handle_endtag(self, tag):
        """ This method handle end tags.

        It also decrements identical tags count to prevent accidental stop record when
        encounter same tag.

        Keyword arguments:
        tag --  name of the handled HTML tag
        """
        if tag == self.targeted_tag:
            self.similar_tag -= 1
            if self.similar_tag == -1:
                self.inner_targeted_tag = False
        elif tag == "script":
            self.inner_script_tag = False

    def handle_data(self, data):
        """ This method handle data inside tag.

        It record data only if the cursor are inside targeted tag.

        Keyword arguments:
        data  --  content of the handled HTML tag
        """
        if self.inner_targeted_tag and not self.inner_script_tag:
            self.data.append(data.strip())


def collect(url: str, tag="body", attrs: list = None):
    """ Collect HTML content from web.

    Keyword arguments:
    url --  URL to retrieve content and decode to UTF-8
    """
    if attrs is None:
        attrs = []
    fp = urlopen(unidecode(url))
    content = fp.read()
    fp.close()
    html = content.decode("utf8")
    parser = TargetTag(tag, attrs)
    parser.feed(html)
    return ' '.join(parser.data)


if __name__ == "__main__":
    test = collect("https://en.wikipedia.org/wiki/K._T._Thomas_(judge)")
    print(test)
