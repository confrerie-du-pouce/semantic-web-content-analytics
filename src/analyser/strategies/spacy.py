import spacy
from spacy import displacy

languages = {
    "fr": "fr_core_news_sm",
    "en": "en_core_web_sm",
    "de": "de_core_news_sm",
    "xx": "xx_ent_wiki_sm"
}

models = dict()
for name in languages:
    models[name] = spacy.load(languages[name])


def render(text: str, lang: str):
    doc = models[lang](text)
    return displacy.render(doc, style="ent")
