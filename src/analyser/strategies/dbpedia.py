from urllib.parse import quote
import requests


def substr_replace(subject: str, replace: str, start: int, length: int):
    return subject[:start] + replace + subject[start + length:]


def fetch(text: str, lang: str):
    res = requests \
        .get(url='https://api.dbpedia-spotlight.org/{lang}/annotate?text={text}'
             .format(lang=lang, text=quote(text)), headers={'accept': 'application/json'})
    return res


def render(text: str, lang: str):
    res = fetch(text, lang)
    json = res.json()
    rendered = text
    if res.status_code == 503:
        return "<b>503 Service unavailable</b><p>DBPedia-spotlight isn't available now.</p>"
    offset = 0
    for resource in json["Resources"]:
        new = "<a target=\"_blank\" href=\"{url}\">{text}</a>".format(
            text=resource["@surfaceForm"],
            url=resource["@URI"])
        length = int(len(resource["@surfaceForm"]))
        rendered = substr_replace(rendered,
                                  new,
                                  int(resource["@offset"]) + offset,
                                  length)
        offset += len(new) - len(resource["@surfaceForm"])
    return rendered
