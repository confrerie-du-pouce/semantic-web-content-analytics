from django.shortcuts import render
from .collector import web
from .strategies import spacy, dbpedia, aidalight


def analyzer(req):
    """ This method prepare data for analysis."""
    analyzed = None
    if len(req.POST) > 0:
        lang = req.POST['lang']
        strategy = req.POST['strategy']
        user_input = req.POST['input']
        system = req.POST['system']
        if strategy == "web":
            user_input = web.collect(user_input)
        if system == "spacy":
            analyzed = spacy.render(user_input, lang)
        elif system == "dbpedia":
            analyzed = dbpedia.render(user_input, lang)
        elif system == "aida":
            analyzed = aidalight.render(user_input, lang)
    return render(req, 'layout.html', {
        "analyzed": analyzed
    })
