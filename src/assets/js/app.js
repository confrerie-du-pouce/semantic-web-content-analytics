const lang = document.getElementById("language");
const system = document.getElementById("system");
const strategy = document.getElementById("strategy");
const input = document.getElementById("input");

function updateSystemRequirements() {
    switch (system.options[system.selectedIndex].value) {
        case "spacy":
            setOptions(lang, {
                "German": "de",
                "English": "en",
                "French": "fr",
                "Wikipedia Entities": "xx"
            });
            setOptions(strategy, {
                "Direct input": "input",
                "Web content": "web"
            });
            break;
        case "dbpedia":
            setOptions(lang, {
                "Danish": "da",
                "German": "de",
                "English": "en",
                "Spanish": "es",
                "French": "fr",
                "Hungarian": "hu",
                "Italian": "it",
                "Portuguese": "pt",
                "Russian": "ru",
                "Swedish": "sv",
                "Turkish": "tr"
            });
            setOptions(strategy, {
                "Direct input": "input"
            });
            break;
    }
}

function removeOptions(select) {
    while (select.length > 0) select.remove(0);
}

function setOptions(select, options) {
    removeOptions(select);
    for (const option in options) {
        const id = options[option];
        select.add(new Option(option, id));
    }
}

function updateTypeInput() {
    switch (strategy.options[strategy.selectedIndex].value) {
        case "web":
            input.type = "url";
            input.placeholder = "http://website.do/page"
            break;
        case "input":
            input.type = "text";
            input.placeholder = "Type your text here."
            break;
    }
}

updateTypeInput();
strategy.onchange = updateTypeInput;

updateSystemRequirements();
system.onchange = updateSystemRequirements;
